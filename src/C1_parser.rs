use std::fs::File;
use crate::{C1Lexer, C1Token, lexer, ParseResult};

pub struct C1Parser<'a>  {
    lx: C1Lexer<'a>,
}

impl<'a> C1Parser<'a>  {
    fn new(text: &str) -> C1Parser {
        C1Parser {
            lx: C1Lexer::new(text),
        }
    }

    pub fn parse(text: &str) -> ParseResult {
        let mut instance = C1Parser::new(text);

        println!("Starting parsing");

        instance.program()
    }

    fn program(&mut self) -> ParseResult {
        while self.lx.current_token().is_some() {
            self.functiondefinition()?;
        }

        Ok(())
    }

    fn functiondefinition(&mut self) -> ParseResult {
        if self.lx.current_token() == Some(C1Token::KwInt) || self.lx.current_token() == Some(C1Token::KwVoid) || self.lx.current_token() == Some(C1Token::KwBoolean) || self.lx.current_token() == Some(C1Token::KwFloat) {
            self.lx.eat();

            self.check_and_eat_token(C1Token::Identifier)?;
            self.check_and_eat_token(C1Token::LeftParenthesis)?;
            self.check_and_eat_token(C1Token::RightParenthesis)?;
            self.check_and_eat_token(C1Token::LeftBrace)?;
            self.statementlist()?;
            return self.check_and_eat_token(C1Token::RightBrace);
        }
        return Err(format!("Expected token group 'type' in line {} but found {:?}", self.lx.current_line_number().unwrap(), self.lx.current_token()));
    }

    fn statementlist(&mut self) -> ParseResult {
        print!("statementlist->");
        //self.block().unwrap(); //TODO: While loop

        /*if self.lx.current_token() == Some(C1Token::LeftBrace) && self.lx.peek_token() == Some(C1Token::RightBrace) {
            self.lx.eat();
            self.lx.eat();
            println!("eat");
            return Ok(());
        }


        if self.lx.current_token() == Some(C1Token::LeftBrace){
            self.check_and_eat_token(C1Token::LeftBrace).unwrap();

            while !(self.lx.current_token() == Some(C1Token::RightBrace)) {
                println!("r");

                if self.lx.current_token() == Some(C1Token::LeftBrace){
                    self.statementlist().unwrap();
                }

                self.statement().unwrap();
            }
            println!("block finished!!!");

            self.check_and_eat_token(C1Token::RightBrace).unwrap();
        }*/

        while !(self.lx.current_token() == Some(C1Token::RightBrace)) {
            self.block()?;
        }

        Ok(())
    }

    fn block(&mut self) -> ParseResult {
        print!("block->");
        if self.lx.current_token() == Some(C1Token::LeftBrace){
            self.check_and_eat_token(C1Token::LeftBrace)?;
            self.statementlist()?;
            return self.check_and_eat_token(C1Token::RightBrace);
        }else{
            return self.statement();
        }
    }

    fn statement(&mut self) -> ParseResult {
        print!("statement->");
        match self.lx.current_token() {
            Some(C1Token::KwIf) => {
                return self.ifstatement();
            },
            Some(C1Token::KwReturn) => {
                self.returnstatement()?;
                return self.check_and_eat_token(C1Token::Semicolon);
            },
            Some(C1Token::KwPrintf) => {
                self.printfstatement()?;
                return self.check_and_eat_token(C1Token::Semicolon);
            },
            Some(C1Token::Identifier) => {
                if self.lx.peek_token() == Some(C1Token::Assign) {
                    self.statassignment()?;
                    return self.check_and_eat_token(C1Token::Semicolon);
                }else{
                    self.functioncall()?;
                    return self.check_and_eat_token(C1Token::Semicolon);
                }
            },
            _ => Err(format!("Expected token group 'statement' in line {} but found {:?}", self.lx.current_line_number().unwrap(), self.lx.current_token().unwrap())),
        }
    }

    fn functioncall(&mut self) -> ParseResult {
        print!("functioncall->");
        self.check_and_eat_token(C1Token::Identifier)?;
        self.check_and_eat_token(C1Token::LeftParenthesis)?;
        self.check_and_eat_token(C1Token::RightParenthesis)
    }

    fn statassignment(&mut self) -> ParseResult {
        print!("statassignment->");
        self.check_and_eat_token(C1Token::Identifier)?;
        self.check_and_eat_token(C1Token::Assign)?;
        self.assignment()
    }


    fn returnstatement(&mut self) -> ParseResult {
        print!("returnstatement->");
        self.check_and_eat_token(C1Token::KwReturn)?;
        if self.lx.current_token() == Some(C1Token::Semicolon) {
            return Ok(());
        }

        self.assignment()
    }

    fn printfstatement(&mut self) -> ParseResult {
        print!("printfstatement->");
        self.check_and_eat_token(C1Token::KwPrintf)?;
        self.check_and_eat_token(C1Token::LeftParenthesis)?;
        self.assignment()?;
        self.check_and_eat_token(C1Token::RightParenthesis)
    }

    fn ifstatement(&mut self) -> ParseResult {
        print!("ifstatement->");
        self.check_and_eat_token(C1Token::KwIf)?;
        self.check_and_eat_token(C1Token::LeftParenthesis)?;
        self.assignment()?;
        self.check_and_eat_token(C1Token::RightParenthesis)?;
        self.block()
    }

    fn assignment(&mut self) -> ParseResult {
        print!("assignment->");
        println!("test: {:?} {:?}", self.lx.current_text(), self.lx.peek_text());
        if self.lx.current_token() == Some(C1Token::Identifier) && self.lx.peek_token() == Some(C1Token::Assign) {
            self.check_and_eat_token(C1Token::Identifier)?;
            self.check_and_eat_token(C1Token::Assign)?;
            self.assignment()
        }else{
            println!("assignment: Not Identifier");
            self.expr()
        }
    }

    fn expr(&mut self) -> ParseResult {
        print!("expr->");
        self.simpexpr()?;

        let tk = self.lx.current_token();
        if tk == Some(C1Token::Equal) || tk == Some(C1Token::NotEqual) || tk == Some(C1Token::Less) || tk == Some(C1Token::LessEqual) || tk == Some(C1Token::Greater) || tk == Some(C1Token::GreaterEqual) {
            self.lx.eat();
            self.simpexpr()?;
        }
        
        Ok(())
    }

    fn simpexpr(&mut self) -> ParseResult {
        print!("simpexpr->");
        if self.lx.current_token() == Some(C1Token::Minus){
            self.check_and_eat_token(C1Token::Minus)?;
        }
        self.term()?;

        while self.lx.current_token() == Some(C1Token::Plus) || self.lx.current_token() == Some(C1Token::Minus) || self.lx.current_token() == Some(C1Token::Or)  {
            self.lx.eat();
            self.term()?;
        }
        Ok(())
    }

    fn term(&mut self) -> ParseResult {
        print!("term->");
        self.factor()?;

        while self.lx.current_token() == Some(C1Token::Asterisk) || self.lx.current_token() == Some(C1Token::Slash) || self.lx.current_token() == Some(C1Token::And) {
            self.lx.eat();
            self.factor()?;
        }
        Ok(())
    }

    fn factor(&mut self) -> ParseResult {
        print!("factor->");



        match self.lx.current_token() {
            Some(C1Token::ConstInt) => {
                self.check_and_eat_token(C1Token::ConstInt)
            },
            Some(C1Token::ConstFloat) => {
                self.check_and_eat_token(C1Token::ConstFloat)
            },
            Some(C1Token::ConstBoolean) => {
                self.check_and_eat_token(C1Token::ConstBoolean)
            },
            Some(C1Token::Identifier) => {

                if self.lx.peek_token() == Some(C1Token::LeftParenthesis) {
                    return self.functioncall();
                }else{
                    return self.check_and_eat_token(C1Token::Identifier);
                }
            },
            Some(C1Token::LeftParenthesis) => {

                print!("factor left parenthesis->");

                self.check_and_eat_token(C1Token::LeftParenthesis)?;
                self.assignment()?;
                self.check_and_eat_token(C1Token::RightParenthesis)?;
                Ok(())
            },
            _ => Err(format!("Expected token group 'factor' in line {} but found {:?}", self.lx.current_line_number().unwrap(), self.lx.current_token().unwrap())),
        }

    }


    fn current_matches(&self, token: C1Token) -> ParseResult {
        if self.lx.current_token() == Some(token) {
            Ok(())
        } else {
            Err(format!("Expected token {:?} in line {}", token, self.lx.current_line_number().unwrap()))
        }
    }

    fn check_and_eat_token(&mut self, token: C1Token) -> ParseResult{
        println!("Checking token: {:?} with text {:?} in line {:?}", token, self.lx.current_text().unwrap(), self.lx.current_line_number().unwrap());
        match self.lx.current_token() {
            Some(x) => {
                if x == token {
                    println!("Correct: {:?}", x);
                    self.lx.eat();
                    return Ok(());
                }
                println!("Wrong: {:?}", x);
                return Err(format!("Expected token {:?} in line {} but found {:?}", token, self.lx.current_line_number().unwrap(), x));
            },
            None => Err("No token".to_owned()),
        }
    }
}